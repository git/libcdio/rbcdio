#!/usr/bin/env ruby
# $Id: isotest.rb,v 1.8 2007/10/13 09:23:17 rocky Exp $
# Unit test for iso9660

# Test some low-level ISO9660 routines
# This is basically the same thing as libcdio's testiso9660.c

require "test/unit"
unless defined?(Mypath)
  Mypath  = File.expand_path(File.dirname(__FILE__))
end
$: << Mypath + '/../lib'
$: << Mypath + '/../ext/cdio'
$: << Mypath + '/../ext/iso9660'

require "iso9660"

def eq?(a, b)
    if a.length() != b.length() then return false end
    
    for i in 0 .. a.length() - 1
        if a[i] != b[i]
            puts "position %d: %d != %d\n" % [i, a[i], b[i]]
            return false
        end
    end
    return true
end
    
class ISO9660Tests < Test::Unit::TestCase

    @@achars = ['!', '"', '%', '&', '(', ')', '*', '+', ',', '-', '.',
                '/', '?', '<', '=', '>']

    # ACHAR and DCHAR
    def test_chars()
      bad = 0

      if 'A'.respond_to?(:ord)
        # Ruby 1.9
        c    = 'A'.ord
        zed  = 'Z'.ord
        zero = '0'.ord
        nine = '9'.ord
        puts zed
      else
        # Ruby 1.8
        c    = ?A
        zed  = ?Z
        zero = ?0
        nine = ?9
      end

      while c <= zed
        if not ISO9660.dchar?(c)
          puts "Failed ISO9660.achar? test on %c" % c
          bad += 1
        end
        if not ISO9660.achar?(c)
          puts "Failed ISO9660.achar? test on %c" % c
          bad += 1
        end
        c += 1
      end

      assert_equal(true, bad==0, 'dchar? & achar? A..Z')
      bad=0
      c = zero
      while c<= nine
        if not ISO9660.dchar?(c)
          puts "Failed ISO9660.dchar? test on %c" % c
          bad += 1
        end
        if not ISO9660.achar?(c)
          puts "Failed ISO9660.achar? test on %c" % c
          bad += 1
        end
        c += 1
      end
      assert_equal(true, bad==0, 'dchar? & achar? 0..9')
      
      bad=0
      i=0
      while i<=13
        if @@achars[i][0].respond_to?(:ord)
          c = @@achars[i][0].ord
        else
          c = @@achars[i][0]
        end
        if ISO9660.dchar?(c)
          puts "Should not pass dchar? test on %c" % c
          bad += 1
        end
        if not ISO9660.achar?(c)
          puts "Failed achar? test on symbol %c" % c
          bad += 1
        end
        i += 1
      end
      
      assert_equal(true, bad==0, 'dchar? & achar? symbols')
    end # test_chars
    
    # Test ISO9660.strncpy_pad
    def test_strncpy_pad()
      
      dst = ISO9660.strncpy_pad("1_3", 5, :dchars)
      assert_equal(dst, "1_3  ", "strncpy_pad DCHARS")
      
      dst = ISO9660.strncpy_pad("ABC!123", 2, :achars)
      assert_equal(dst, "AB", "strncpy_pad ACHARS truncation")
    end
    
    # Test ISO9660.dirname_valid?
    def test_dirname()
      
      assert_equal(false, ISO9660.dirname_valid?("/NOGOOD"),
                   "dirname_valid? - /NOGOOD is no good.")
      
      assert_equal(false, 
                   ISO9660.dirname_valid?("LONGDIRECTORY/NOGOOD"),
                   "pyiso9660.dirname_valid? - too long directory")
      
      assert_equal(true, ISO9660.dirname_valid?("OKAY/DIR"),
                   "dirname_valid? - OKAY/DIR should pass ")
      
      assert_equal(false, ISO9660.dirname_valid?("OKAY/FILE.EXT"),
                   "dirname_valid? - OKAY/FILENAME.EXT")
    end # test_dirname
    
    # Test retrieving image information
    def test_image_info()
      
      # The test ISO 9660 image
      image_path= Mypath + "/../data"
      image_fname=image_path +  "/copying.iso"
      iso = ISO9660::IFS::new(image_fname)
      assert(iso != nil, "Opening %s" % image_fname)
      
      assert_equal(iso.application_id(),
                   "MKISOFS ISO 9660/HFS FILESYSTEM BUILDER & CDRECORD CD-R/DVD CREATOR (C) 1993 E.YOUNGDALE (C) 1997 J.PEARSON/J.SCHILLING",
                   "get_application_id()")
      
      assert_equal(iso.system_id(), "LINUX",
                   "get_system_id() eq 'LINUX'")
      assert_equal(iso.volume_id(), "CDROM",
                   "get_volume_id() eq 'CDROM'")
      
      file_stats = iso.readdir('/')
      
      okay_stats = [
                    {"size"=>2048, "type"=>2, "secsize"=>1, "lsn"=>23, 
                      "filename"=>"."},
                    {"size"=>2048, "type"=>2, "secsize"=>1, "lsn"=>23, 
                      "filename"=>".."},
                    {"size"=>17992, "type"=>1, "secsize"=>9, "lsn"=>24, 
                      "filename"=>"COPYING.;1"}
                   ] 
      assert_equal(file_stats, okay_stats, "file stat info")
    end # test_image_info
    
    # Test pathname_valid?
    def test_pathname_valid()
      # require "debug"
      assert_equal(true, ISO9660.pathname_valid?("OKAY/FILE.EXT"),
                   "pyiso9660.dirname_valid? - OKAY/FILE.EXT ")
      assert_equal(false, 
                   ISO9660.pathname_valid?("OKAY/FILENAMELONG.EXT"),
                   'invalid pathname, long basename')
      
      assert_equal(false, 
                   ISO9660.pathname_valid?("OKAY/FILE.LONGEXT"),
                   "pathname_valid? - long extension" )
      
      dst = ISO9660.pathname_isofy("this/file.ext", 1)
      assert(dst != "this/file.ext1", "ISO9660.pathname_isofy")
    end # test_pathname_valid
    
#     # Test time
#     def test_time()
#         import time

#         tm = time.localtime(0)
#         dtime = set_dtime(tm[0], tm[1], tm[2], tm[3], tm[4], tm[5])
#         new_tm = dtime(dtime, true)

#         ### FIXME Don't know why the discrepancy, but there is an hour
#         ### difference, perhaps daylight savings time.
#         ### Versions before 0.77 have other bugs.
#         if new_tm
#             # if Rubyiso9660::VERSION_NUM < 77 then new_tm[3] = tm[3] end
#             new_tm[3] = tm[3]
#             assert_equal(true, eq?(new_tm, tm), 'get_dtime(set_dtime())')
#         else
#             assert_equal(true, false, 'get_dtime is None')
#         end

# #        if Rubyiso9660::VERSION_NUM >= 77:
# #            tm = time.gmtime(0)
# #            ltime = set_ltime(tm[0], tm[1], tm[2], tm[3], tm[4],
# #                                        tm[5])
# #            new_tm =  get_ltime(ltime)
# #            assert_equal(true, eq?(new_tm, tm), 
# #                        'get_ltime(set_ltime())')
#     end

end # ISO9660Tests


#!/usr/bin/env ruby

# Unit test for CD-Text

require 'test/unit'
mypath  = File.expand_path(File.dirname(__FILE__))

%w(lib ext/cdio).each do |d|
  $: << File.join(mypath, '..', d)
end
%w(cdio rubycdio).each {|f| require f}

class CDTextTests < Test::Unit::TestCase
  
  # Test keywords
  def test_keyword
    if Rubycdio::VERSION_NUM <= 83
      assert_equal("PERFORMER",
                   Rubycdio.cdtext_field2str(Rubycdio::CDTEXT_PERFORMER))
    else
      assert_equal("PERFORMER",
                   Rubycdio.cdtext_field2str(Rubycdio::CDTEXT_FIELD_PERFORMER))
    end
  end
  
  # Test getting CD-Text
  def test_get_set
    tocpath = File.join(File.dirname(__FILE__), 'cdtext.toc')
    device = Cdio::Device.new(tocpath, Rubycdio::DRIVER_CDRDAO)

    if Rubycdio::VERSION_NUM <= 83
      disctext = device.track(0).cdtext()
      assert_equal('Performer', disctext.get(Rubycdio::CDTEXT_PERFORMER))
      assert_equal('CD Title', disctext.get(Rubycdio::CDTEXT_TITLE))
      assert_equal('XY12345', disctext.get(Rubycdio::CDTEXT_DISCID))

      track1text = device.track(1).cdtext()
      assert_equal('Performer', track1text.get(Rubycdio::CDTEXT_PERFORMER))
      assert_equal('Track Title', track1text.get(Rubycdio::CDTEXT_TITLE))
    else
      text = device.cdtext()
      assert_equal('Performer', text.get(Rubycdio::CDTEXT_FIELD_PERFORMER, 0))
      assert_equal('CD Title', text.get(Rubycdio::CDTEXT_FIELD_TITLE, 0))
      assert_equal('XY12345', text.get(Rubycdio::CDTEXT_FIELD_DISCID, 0))

      print text.get(Rubycdio::CDTEXT_FIELD_PERFORMER, 0)
      assert_equal('Performer', text.get(Rubycdio::CDTEXT_FIELD_PERFORMER, 1))
      assert_equal('Track Title', text.get(Rubycdio::CDTEXT_FIELD_TITLE, 1))
    end
  end
end


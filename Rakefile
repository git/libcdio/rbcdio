#!/usr/bin/env rake
# -*- Ruby -*-
require 'rubygems'

ROOT_DIR = File.dirname(__FILE__)

def gemspec
  @gemspec ||= eval(File.read('.gemspec'), binding, '.gemspec')
end

require 'rake/gempackagetask'
desc "Build the gem"
task :package=>:gem
task :gem=>:gemspec do
  Dir.chdir(ROOT_DIR) do
    sh "gem build .gemspec"
    FileUtils.mkdir_p 'pkg'
    FileUtils.mv "#{gemspec.name}-#{gemspec.version}.gem", 'pkg'
  end
end

desc "Install the gem locally"
task :install => :gem do
  Dir.chdir(ROOT_DIR) do
    sh %{gem install --local pkg/#{gemspec.name}-#{gemspec.version}}
  end
end

desc 'Create a GNU-style ChangeLog via git2cl'
task :ChangeLog do
  system('git log --pretty --numstat --summary | git2cl > ChangeLog')
end

require 'rake/testtask'
desc "Test everything."
Rake::TestTask.new('test') do |t|
  t.pattern = 'test/*.rb'
  t.warning = true
end
task :test => [:lib, :all]

desc "same as test"
task :check => :test

desc "Generate the gemspec"
task :generate do
  puts gemspec.to_ruby
end

desc "Validate the gemspec"
task :gemspec do
  gemspec.validate
end

# ---------  RDoc Documentation ------
require 'rake/rdoctask'
Rake::RDocTask.new("rdoc") do |rdoc|
  rdoc.rdoc_dir = 'doc'
  rdoc.title    = "rbcdio"
  # Show source inline with line numbers
  rdoc.options += ("--exclude data --exclude test --exclude lib/cdio/ " +
	           "--exclude lib/iso9660/ --exclude pkg/" ).split()
  # Make the readme file the start page for the generated html
  rdoc.options << '--main' << 'README'
  rdoc.rdoc_files.include('lib/*.rb',
                           'example/*.rb',
                           'example/README',
                           'README')
end
desc "Same as rdoc"
task :doc => :rdoc

task :default => :test

# --- Redo Rake::PackageTask::define so tar uses -h to include
# files of a symbolic link.
module Rake
  class PackageTask < TaskLib
    # Create the tasks defined by this task library.
    def define
      fail "Version required (or :noversion)" if @version.nil?
      @version = nil if :noversion == @version

      desc "Build all the packages"
      task :package
      
      desc "Force a rebuild of the package files"
      task :repackage => [:clobber_package, :package]
      
      desc "Remove package products" 
      task :clobber_package do
	rm_r package_dir rescue nil
      end

      task :clobber => [:clobber_package]

      [
	[need_tar, tgz_file, "z"],
	[need_tar_gz, tar_gz_file, "z"],
	[need_tar_bz2, tar_bz2_file, "j"]
      ].each do |(need, file, flag)|
	if need
	  task :package => ["#{package_dir}/#{file}"]
	  file "#{package_dir}/#{file}" => [package_dir_path] + package_files do
	    chdir(package_dir) do
	      sh %{tar #{flag}hcvf #{file} #{package_name}}
	    end
	  end
	end
      end
      
      if need_zip
	task :package => ["#{package_dir}/#{zip_file}"]
	file "#{package_dir}/#{zip_file}" => [package_dir_path] + package_files do
	  chdir(package_dir) do
	    sh %{zip -r #{zip_file} #{package_name}}
	  end
	end
      end

      directory package_dir

      file package_dir_path => @package_files do
	mkdir_p package_dir rescue nil
	@package_files.each do |fn|
	  f = File.join(package_dir_path, fn)
	  fdir = File.dirname(f)
	  mkdir_p(fdir) if !File.exist?(fdir)
	  if File.directory?(fn)
	    mkdir_p(f)
	  else
	    rm_f f
	    safe_ln(fn, f)
	  end
	end
      end
      self
    end
  end
end

desc "Create shared objects"
task :all do
  sh "./configure"
  sh "make"
end

desc "Clear temp files"
task :clean do
  sh "make clean"
end

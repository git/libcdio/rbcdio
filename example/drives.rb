#!/usr/bin/env ruby
#
#  Program to list CD drives and analyze the media type in them

#  Copyright (C) 2006, 2007, 2008 Rocky Bernstein <rocky@gnu.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

mypath = File.dirname(__FILE__)
if(File::exists?(mypath + "/../lib/cdio.rb"))
  $: << File.dirname(__FILE__) + '/../ext/cdio'
  $: << File.dirname(__FILE__) + '/../lib'
else
  require 'rubygems'
end
require "cdio"

def print_drive_class(msg, bitmask, any)
    cd_drives = Rubycdio::get_devices_with_cap(bitmask, any)

    puts "%s..." % msg
    for drive in cd_drives
	puts "Drive %s" % drive
    end if cd_drives
    puts "-----"
end

cd_drives = Cdio::devices(Rubycdio::DRIVER_DEVICE)
if not cd_drives
    puts "No CD-ROM drives found"
  exit
end
for drive in cd_drives
    puts "Drive %s" % drive
end

puts "-----"

print_drive_class("All CD-ROM drives (again)", Rubycdio::FS_MATCH_ALL, false)
print_drive_class("All CD-DA drives...", Rubycdio::FS_AUDIO, false)
print_drive_class("All drives with ISO 9660...", Rubycdio::FS_ISO_9660, false)
print_drive_class("VCD drives...", 
                  (Rubycdio::FS_ANAL_SVCD | Rubycdio::FS_ANAL_CVD |
                   Rubycdio::FS_ANAL_VIDEOCD | Rubycdio::FS_UNKNOWN), 
                  true);




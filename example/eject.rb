#!/usr/bin/env ruby
#  
# A program to eject and close CD-ROM drive

#  Copyright (C) 2006, 2007, 2008 Rocky Bernstein <rocky@gnu.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

mypath = File.dirname(__FILE__)
if(File::exists?(mypath + "/../lib/cdio.rb"))
  $: << File.dirname(__FILE__) + '/../lib'
  $: << File.dirname(__FILE__) + '/../ext/cdio'
else
  require 'rubygems'
end
require "cdio"

if ARGV.length() > 0
    begin
        drive_name=@ARGV[1]
        d = Cdio::Device.new(drive_name)
    rescue IOError
        print "Problem opening CD-ROM: %s" % drive_name
        exit 1
    end
else
    begin
        d = Cdio::Device.new(nil, Rubycdio::DRIVER_UNKNOWN)
        drive_name = d.device()
    rescue IOError
        print "Problem finding a CD-ROM"
        exit 1
    end
end

begin
    puts "Sending eject to drive %s" % drive_name
    d.eject_media()
    begin
        puts "Closing CD in drive %s" % drive_name
        Cdio::close_tray(drive_name)
        puts "Closed tray of CD-ROM drive %s" % drive_name
    rescue Cdio::DeviceException
        puts "Closing tray of CD-ROM drive %s failed" % drive_name
    end

rescue Cdio::DriverUnsupportedError
    puts "Eject not supported for %s" % drive_name
rescue Cdio::DeviceException
    puts "Eject of CD-ROM drive %s failed" % drive_name
end

    

  

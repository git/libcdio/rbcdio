#!/usr/bin/env ruby
# $Id: cdchange.rb,v 1.3 2008/05/02 13:05:39 karl Exp $ -*- Python -*-
# 
# A Program to show CD media changing

#  Copyright (C) 2007, 2008 Rocky Bernstein <rocky@gnu.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

mypath = File.dirname(__FILE__)

if(File::exists?(mypath + "/../lib/cdio.rb"))
  $: << File.dirname(__FILE__) + '/../lib'
  $: << File.dirname(__FILE__) + '/../ext/cdio'
else
  require 'rubygems'
end
require "cdio"

sleep_time = 15
if ARGV[0]
    begin
        d = Cdio::Device.new(ARGV.first)
    rescue IOError
      print "Problem opening CD-ROM: %s" % sys.argv[1]
      exit 1
    end
    if ARGV[1]
      begin
        sleep_time = ARGV[1].to_i
      rescue
        print "Invalid sleep parameter #{ARGV[1]}"
        exit 2
      end
    end
else
    begin
        d = Cdio::Device.new(nil, Rubycdio::DRIVER_UNKNOWN)
    rescue IOError
      print "Problem finding a CD-ROM"
      exit 1
    end
end

if d.media_changed?
  print "Initial media status: changed"
else
  print "Initial media status: not changed"
end

print "Giving you #{sleep_time} seconds to change CD if you want to do so."
sleep(sleep_time)
if d.media_changed?
  print "Media status: changed"
else
    print "Media status: not changed"
end
d.close()

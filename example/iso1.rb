#!/usr/bin/env ruby
#
# A simple program to show using libiso9660 to list files in a directory of
# an ISO-9660 image.
#
# If a single argument is given, it is used as the ISO 9660 image to
# use in the listing. Otherwise a compiled-in default ISO 9660 image
# name (that comes with the libcdio distribution) will be used.

#  Copyright (C) 2006, 2007, 2008 Rocky Bernstein <rocky@gnu.org>
#  
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

mypath = File.dirname(__FILE__)
if(File::exists?(mypath + "/../lib/cdio.rb"))
  $: << File.dirname(__FILE__) + '/../lib'
  $: << File.dirname(__FILE__) + '/../ext/cdio'
  $: << File.dirname(__FILE__) + '/../ext/iso9660'
else
  require 'rubygems'
end
  
require "iso9660"

# The default ISO 9660 image if none given
ISO9660_IMAGE_PATH="../data"
ISO9660_IMAGE=ISO9660_IMAGE_PATH + "/copying.iso"
if ARGV.length() > 1
    iso_image_fname = sys.argv[1]
else
    iso_image_fname = ISO9660_IMAGE
end

iso = ISO9660::IFS.new(iso_image_fname)
  
if not iso.open?()
    puts "Sorry, couldn't open %s as an ISO-9660 image." %  iso_image_fname
    exit(1)
end

path = '/'

file_stats = iso.readdir(path)

id_str = iso.application_id()
if id_str then puts "Application ID: %s" % id_str end

id_str = iso.preparer_id()
if id_str then puts "Preparer ID: %s" % id_str end

id_str = iso.publisher_id()
if id_str then puts "Publisher ID: %s" % id_str end

id_str = iso.system_id()
if id_str then puts "System ID: %s" % id_str end

id_str = iso.volume_id()
if id_str then puts "Volume ID: %s" % id_str end

id_str = iso.volumeset_id()
if id_str then puts "Volumeset ID: %s" % id_str end

for stat in file_stats
    filename = stat["filename"]
    lsn      = stat["lsn"]
    size     = stat["size"]
    sec_size = stat["secsize"]
    is_dir   = stat["type"] == 2 ? 'd' : '-'
    puts "%s [LSN %6d] %8d %s%s" % [is_dir, lsn, size, path,
                                    ISO9660.name_translate(filename)]
end
iso.close()
exit(0)



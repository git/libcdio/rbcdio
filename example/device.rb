#!/usr/bin/env ruby
#
# A Program to show CD-ROM device information

#  Copyright (C) 2006, 2007, 2008 Rocky Bernstein <rocky@gnu.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

mypath = File.dirname(__FILE__)
if(File::exists?(mypath + "/../lib/cdio.rb"))
  $: << File.dirname(__FILE__) + '/../ext/cdio'
  $: << File.dirname(__FILE__) + '/../lib'
else
  require 'rubygems'
end
require "cdio"

#---
# Return sorted keys of a dictionary.
# There's probably an easier way to do this that I'm not aware of.
def sort_dict_keys(dict)
    keys=dict.keys()
    keys.sort()
    return keys
end

# Show Available drivers
def driver_availability()
  puts "\nDriver Availabiliity..."
  drivers.each_pair { |driver_name, driver_id|
    begin 
      if driver?(driver_id):
          puts "Driver %s is installed." % driver_name
      end
    rescue ValueError
    end
  }
end


if ARGV.length() > 0
    begin
        drive_name = ARGV[0]
        d = Cdio::Device.new(drive_name)
    rescue IOError
        puts "Problem opening CD-ROM: %s" % drive_name
        exit(1)
    end
else
    begin
      d = Cdio::Device.new("", Rubycdio::DRIVER_UNKNOWN)
      drive_name = d.device()
    rescue IOError:
      puts "Problem finding a CD-ROM"
      driver_availability
      exit
    end
end

# Should there should be no "ok"?
hw  = d.hwinfo()

if hw then
  puts "drive: %s, vendor: %s, model: %s, revision: %s" \
  % [drive_name, hw["vendor"], hw["model"], hw["revision"]]
end

read_cap, write_cap, misc_cap = d.drive_cap()
puts "Drive Capabilities for %s..." % drive_name

# FIXME
# puts "\n".join(cap for cap in sort_dict_keys(read_cap) +
#                sort_dict_keys(write_cap) + sort_dict_keys(misc_cap))

d.close()

driver_availability

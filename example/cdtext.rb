#!/usr/bin/env ruby
# Program to show cdtext, similar to examples/cdtext.c
#
#  Copyright (C) 2006, 2008, 2009, 2012 Rocky Bernstein <rocky@gnu.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# require 'rubygems'; require 'trepanning'

mypath  = File.expand_path(File.dirname(__FILE__))

%w(lib ext/cdio).each do |d|
  $: << File.join(mypath, '..', d)
end
%w(cdio rubycdio).each {|f| require f}

def print_cdtext_track_info(text, track)
  for i in Rubycdio::MIN_CDTEXT_FIELD.upto(Rubycdio::MAX_CDTEXT_FIELDS-1) do
    if Rubycdio::VERSION_NUM <= 83
      value = text.get(i)
    else
      value = text.get(i, track)
    end
    # value can be empty but exist, compared to NULL values
    unless value.nil?
      puts "\t%s: %s" % [Rubycdio::cdtext_field2str(i), value]
    end
  end
  puts
end

if ARGV.size > 0 
  begin
    drive_name = ARGV[0]
    d = Cdio::Device.new(drive_name)
  rescue IOError
    puts "Problem opening CD-ROM: %s" % drive_name
   exit 1
  end
else
  begin
    d = Cdio::Device.new(nil, Rubycdio::DRIVER_UNKNOWN)
    drive_name = d.device()
  rescue IOError
    puts "Problem finding a CD-ROM"
    exit 1
  end
end

i_tracks = d.num_tracks()
t = d.first_track()
i_first_track = t.track

if Rubycdio::VERSION_NUM <= 83
  text = d.track(0).cdtext()
else
  text = d.cdtext()
end

print "CD-Text for Disc:"
print_cdtext_track_info(text, 0)

for i in i_first_track.upto(i_tracks + i_first_track -1) do
  if Rubycdio::VERSION_NUM <= 83
    text = d.track(i).cdtext()
  end
  print "CD-Text for Track %d:" %i
  print_cdtext_track_info(text, i)
end

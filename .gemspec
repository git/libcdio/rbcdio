# ---------  GEM package ------
require 'rubygems'
require 'rake'
require 'rubygems' unless 
  Object.const_defined?(:Gem)

CDIO_VERSION = open("VERSION").read.chomp

FILES = FileList[
  'AUTHORS',
  'COPYING',
  'ChangeLog',
  'INSTALL',
  'Makefile.am',
  'Makefile.in',
  'NEWS',
  'README',
  'Rakefile',
  'THANKS',
  'VERSION',
  'VERSION.in',
  'config.guess',
  'config.sub',
  'configure',
  'configure.ac',
  'data/**/*',
  'doc/*',
  'example/*',
  'ext/**/*.c',
  'ext/**/Makefile',
  'ext/**/extconf.rb',
  'install-sh',
  'lib/*.rb',
  'lib/Makefile',
  'missing',
  'rubycdio.m4',
  'swig/*.swg',
  'swig/Makefile',
  'test/*.rb',
  'test/Makefile',
  'test/Rakefile',
  'test/cdda.bin',
  'test/cdda.cue',
  'test/cdda.toc'
]

desc "Create GEM spec file"
spec = Gem::Specification.new do |spec|
  spec.authors      = ['Rocky Bernstein']
  spec.bindir        = 'bin'
  spec.date         = Time.now
  spec.email        = "rockyb@rubyforge.org"
  spec.files         = FILES.to_a  
  spec.executables   = []
  spec.extensions    = ["ext/cdio/extconf.rb", "ext/iso9660/extconf.rb"]
  
  spec.homepage = "http://rubyforge.org/projects/rbcdio/"
  spec.description = <<-EOF
A library for CD-ROM and CD image access. Applications wishing to be
oblivious of the OS- and device-dependent properties of a CD-ROM or of
the specific details of various CD-image formats may benefit from
using this library. A library for working with ISO-9660 filesystems
is included.
EOF

  spec.name          = 'rbcdio'
  spec.platform      = Gem::Platform::RUBY
  spec.require_path  = 'lib'
  spec.test_files = FileList['tests/**/*']

  spec.required_ruby_version = '>= 1.8.2'
  spec.rubyforge_project = 'rbcdio'
  spec.summary = 'Ruby binding for libcdio (CD Input and Control library)'
  spec.version = CDIO_VERSION

  # rdoc
  spec.has_rdoc = true
end


require 'mkmf'

dir_config('rubyiso9660')

$LDFLAGS = [
  $LDFLAGS,
  ENV['LDFLAGS'],
  `pkg-config libiso9660 --libs`.strip
].join(' ')

$CFLAGS = [
  $CFLAGS,
  ENV['CFLAGS'],
  `pkg-config libiso9660 --cflags`.strip
].join(' ')

create_makefile('rubyiso9660')

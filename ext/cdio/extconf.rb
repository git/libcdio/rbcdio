require 'mkmf'
dir_config('rubycdio')
require 'rbconfig'

config_file = File.join(File.dirname(__FILE__), 'config_options')               
load config_file if File.exist?(config_file)                                    

$LDFLAGS = [
  $LDFLAGS,
  ENV['LDFLAGS'],
  `pkg-config libcdio --libs`.strip
].join(' ')

$CFLAGS = [
  $CFLAGS,
  ENV['CFLAGS'],
  `pkg-config libcdio --cflags`.strip
].join(' ')

create_makefile('rubycdio')
